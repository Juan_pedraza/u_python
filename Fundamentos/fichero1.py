class Locust():
    texto = ''
    def __init__(self,nombre,tipo):
        self.nombre = nombre
        self.tipo = tipo

    def saludar(self):
        self.texto = f'Hola, soy un locust llamado {self.nombre}'
        return self.texto


berserker = Locust("berserker","guerrero")
saludar = berserker.saludar()

print(berserker.nombre)
print(saludar)


class warrior(Locust):
    def __init__(self, nombre,tipo):
        Locust.__init__(self,nombre,tipo)
    def saludar(self):
        self.texto = f'Soy un guardia {self.nombre} y soy un locust poderoso'
        return self.texto

teron = warrior("Terón","guerrero")
saludar = teron.saludar()
print(saludar)