# Manejo de errores

# En funciones el try-except se maneja por fuera de la función

def sumar(a,b):
    resultado = a + b
    return resultado

try:
    print(sumar(3,5))
except:
    print("Debes poner los valores para realizar la suma")


