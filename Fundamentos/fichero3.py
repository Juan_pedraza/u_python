# Expresiones regulares

import re

texto = "Hay una serpiente en mi bota"
patron = "serpiente"

encontrar = re.search(patron,texto)

if encontrar:
    print(f'El patrón {patron} fue encontrado')
else:
    print(f'El patrón {patron} no fue encontrado')