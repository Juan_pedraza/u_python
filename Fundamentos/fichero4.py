# import module

# # Quiero usar la función suma

# sumar = module.sumar(3,5)

# print(sumar)

# Otra manera de importar solo lo que necesito es:

from module import sumar

suma = sumar(5,8)
print(suma)