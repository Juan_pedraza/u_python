"""
Funciones decoradoras: 

Son funciones que se aplican sobre otra función

"""

def asteriscos(function):
    def poner_asteriscos():
        print("************")
        function()
        print("************")
    return poner_asteriscos

# Se llama la función usando el símbolo de arroba antes de la siguiente función
@asteriscos
def imprimir():
    print("Buenos días")

imprimir()