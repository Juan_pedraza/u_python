from flask import Flask

app = Flask(__name__)

# Rutas fijas
@app.route("/")
def principal():
    return "<h1>Hola mundo</h1>"

@app.route("/bye")
def despedida():
    return "<h2>Bye, See you later!</h2>"

# Rutas Dinámicas

@app.route("/saludar/<name>")
def saludar(name):
    return f"<h2> Hola {name}, te saluda la página con Flask </h2>"

@app.route("/longitud/<name>")
def longitud(name):
    return f"<p> La longitud del nombre {name} es {len(name)} </p>  "

@app.route("/edad/<name>/<age>")
def edad(name,age):
    return f"{name} tiene {age} años"

@app.route("/sumar/<num1>/<num2>")
def sumar(num1,num2):
    num1 = int(num1)
    num2 = int(num2)
    suma = num1 + num2    
    return f"El resultado de sumar {num1} y {num2} es igual a {int(suma)}"


if __name__ == '__main__':
    app.run()
